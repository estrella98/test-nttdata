package com.starcode.framework.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.starcode.framework.room.DBConstants
import com.starcode.framework.room.model.Contact
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface ContactDao {

    @Insert
    fun insert(item:Contact)

    @Query("SELECT * FROM ${DBConstants.Contact.TABLE_NAME} WHERE ${DBConstants.Contact.COLUMN_DOCUMENT_NUMBER}=:value")
    fun getContactByDocumentNumber(value:String): Maybe<Contact>

    @Query("SELECT * FROM ${DBConstants.Contact.TABLE_NAME}")
    fun getAllContacts(): Flowable<List<Contact>>

    @Query("DELETE FROM ${DBConstants.Contact.TABLE_NAME} WHERE ${DBConstants.Contact.COLUMN_ID}=:id")
    fun deleteContact(id: Int?)
}