package com.starcode.framework.room

object DBConstants {

    const val NAME_DB = "database_contacts"

    object Contact {
        const val TABLE_NAME = "contact"
        const val COLUMN_ID = "id"
        const val COLUMN_FULL_NAME= "full_name"
        const val COLUMN_IMAGE = "image"
        const val COLUMN_DOCUMENT_NUMBER = "document_number"
        const val COLUMN_EMAIL = "email"
        const val COLUMN_PHONE_NUMBER = "phone_number"
        const val COLUMN_BIRTHDAY = "birthday"
    }

}