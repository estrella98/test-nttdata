package com.starcode.framework.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.starcode.framework.room.dao.ContactDao
import com.starcode.framework.room.model.Contact

/**
 *To manage data items that can be accessed, updated
 * @Created by ESTRELLA
 */
@Database(
    entities = [Contact::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
//
    abstract fun contactDao(): ContactDao
}