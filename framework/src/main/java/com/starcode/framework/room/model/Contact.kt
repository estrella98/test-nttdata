package com.starcode.framework.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.starcode.framework.room.DBConstants

@Entity(tableName = DBConstants.Contact.TABLE_NAME)
data class Contact(
    @ColumnInfo(name = DBConstants.Contact.COLUMN_FULL_NAME)
    val fullName: String,
    @ColumnInfo(name = DBConstants.Contact.COLUMN_IMAGE)
    val photo: String?,
    @ColumnInfo(name = DBConstants.Contact.COLUMN_DOCUMENT_NUMBER)
    val documentNumber: String,
    @ColumnInfo(name = DBConstants.Contact.COLUMN_EMAIL)
    val email: String,
    @ColumnInfo(name = DBConstants.Contact.COLUMN_PHONE_NUMBER)
    val phoneNumber: String,
    @ColumnInfo(name = DBConstants.Contact.COLUMN_BIRTHDAY)
    val birthday: String,
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.Contact.COLUMN_ID)
    var id: Int = 0
}