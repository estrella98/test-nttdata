package com.starcode.registercontacts.data.source

import android.util.Log
import com.starcode.framework.room.dao.ContactDao
import com.starcode.registercontacts.domain.model.Contact
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ContactDataSource @Inject constructor(private val contactDao: ContactDao): IContactDataSource {

    override fun saveContact(contact: Contact): Maybe <Boolean> {
        val item =  contact.toEntityDb()
        Log.d("TAG", "DOCUMENT :${item.documentNumber}")
        return contactDao.getContactByDocumentNumber(item.documentNumber)
            .isEmpty
            .flatMapMaybe { isEmpty->
            Log.d("TAG", "isEmpty :$isEmpty")
                if(isEmpty){
                    contactDao.insert(item)
                }
                Maybe.just(isEmpty)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    override fun getAllContacts(): Flowable<List<Contact>> = contactDao
        .getAllContacts()
        .map { it.map{ Contact(it.fullName, it.photo, it.documentNumber, it.email,it.phoneNumber, it.birthday, it.id)} }
        .onErrorReturn { emptyList() }
        .subscribeOn(Schedulers.io())

    override fun deleteContact(item: Contact) {
        contactDao.deleteContact(item.id)
    }


}