package com.starcode.registercontacts.data.source

import com.starcode.registercontacts.domain.model.Contact
import io.reactivex.Flowable
import io.reactivex.Maybe

interface IContactDataSource {

    fun saveContact(contact: Contact): Maybe<Boolean>
    fun getAllContacts(): Flowable<List<Contact>>
    fun deleteContact(item: Contact)
}