package com.starcode.registercontacts.data.repository

import com.starcode.registercontacts.data.source.IContactDataSource
import com.starcode.registercontacts.domain.interfaces.IRegisterContactRepository
import com.starcode.registercontacts.domain.model.Contact
import javax.inject.Inject

class RegisterContactRepository @Inject constructor(private val iContactDataSource: IContactDataSource): IRegisterContactRepository {

    override fun saveContact(contact: Contact)= iContactDataSource.saveContact(contact)
    override fun getAllContacts()= iContactDataSource.getAllContacts()
    override fun deleteContact(item: Contact)=iContactDataSource.deleteContact(item)
}