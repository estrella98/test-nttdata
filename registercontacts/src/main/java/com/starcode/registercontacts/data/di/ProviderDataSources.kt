package com.starcode.registercontacts.data.di

import com.starcode.registercontacts.data.source.ContactDataSource
import com.starcode.registercontacts.data.source.IContactDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ProviderDataSources {

    @Provides
    @Singleton
    fun contactDataSourceProvider(contactDataSource: ContactDataSource): IContactDataSource =
        contactDataSource
}