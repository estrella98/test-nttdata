package com.starcode.registercontacts.domain.model

import com.starcode.framework.room.model.Contact

data class Contact(
    val fullName: String,
    val photo: String?,
    val documentNumber: String,
    val email: String,
    val phoneNumber: String,
    val birthday: String,
    val id: Int?
){

    fun toEntityDb()= Contact(fullName, photo, documentNumber, email, phoneNumber, birthday)
}