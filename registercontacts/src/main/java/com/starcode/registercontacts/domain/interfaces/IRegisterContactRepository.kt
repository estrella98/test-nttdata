package com.starcode.registercontacts.domain.interfaces

import com.starcode.registercontacts.domain.model.Contact
import io.reactivex.Flowable
import io.reactivex.Maybe

interface IRegisterContactRepository {

    fun saveContact(contact: Contact): Maybe<Boolean>
    fun getAllContacts(): Flowable<List<Contact>>
    fun deleteContact(item: Contact)
}