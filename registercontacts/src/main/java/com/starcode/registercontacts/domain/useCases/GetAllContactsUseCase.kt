package com.starcode.registercontacts.domain.useCases

import com.starcode.registercontacts.domain.interfaces.IRegisterContactRepository
import com.starcode.registercontacts.domain.model.Contact
import io.reactivex.Flowable
import javax.inject.Inject

class GetAllContactsUseCase @Inject constructor(private val iRegisterContactRepository: IRegisterContactRepository) {

    fun invoke(): Flowable<List<Contact>> = iRegisterContactRepository.getAllContacts()

    fun deleteContact(item:Contact) = iRegisterContactRepository.deleteContact(item)

}