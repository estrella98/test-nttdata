package com.starcode.registercontacts.domain.useCases

import com.starcode.registercontacts.domain.interfaces.IRegisterContactRepository
import com.starcode.registercontacts.domain.model.Contact
import io.reactivex.Maybe
import javax.inject.Inject

class RegisterContactUseCase @Inject constructor(private val iRegisterContactRepository: IRegisterContactRepository) {

    fun invoke(item:Contact): Maybe<Boolean> = iRegisterContactRepository.saveContact(item)
}