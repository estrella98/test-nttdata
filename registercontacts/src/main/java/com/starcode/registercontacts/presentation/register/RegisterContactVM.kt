package com.starcode.registercontacts.presentation.register

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.starcode.registercontacts.domain.model.Contact
import com.starcode.registercontacts.domain.useCases.RegisterContactUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class RegisterContactVM @Inject constructor(private val registerContactUseCase: RegisterContactUseCase) :
    ViewModel() {

    private val disposable = CompositeDisposable()

    private val _contactRegister = MutableLiveData<Boolean>()
    val contactRegister: LiveData<Boolean>
        get() = _contactRegister

    private var imagePath:String?=null

    fun saveImage(path: String?) {
        imagePath = path
    }

    fun saveContact(
        name: String,
        document: String,
        email: String,
        phone: String,
        birthday: String
    ) {
        disposable.add(
            registerContactUseCase
                .invoke(Contact(name, imagePath, document, email, phone, birthday, null))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { isCreated ->
                    _contactRegister.value =
                        isCreated
                }
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }



}