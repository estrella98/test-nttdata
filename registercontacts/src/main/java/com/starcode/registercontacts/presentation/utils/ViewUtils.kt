package com.starcode.registercontacts.presentation.utils

import android.Manifest
import android.content.Context
import android.net.Uri
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy


import com.bumptech.glide.request.RequestOptions
import com.starcode.registercontacts.R


fun showToast(message: String, context: Context) =
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

fun ImageView.showImage(a: FragmentActivity, url: String?) {
    val options: RequestOptions = RequestOptions()
        .centerCrop()
        .placeholder(R.drawable.user)
        .error(R.drawable.user)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .priority(Priority.HIGH)

    if (!url.isNullOrEmpty()) {
        Glide.with(a)
            .load(url).apply(options).into(this)

    }
}

fun ImageView.showImage(a: FragmentActivity, uri: Uri?) {
    setImageURI(uri)
}

fun TextView.show() {
    isVisible = true
}

fun TextView.hide() {
    isVisible = false
}

fun permissionsList() = arrayOf(
    Manifest.permission.READ_EXTERNAL_STORAGE
)