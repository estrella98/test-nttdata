package com.starcode.registercontacts.presentation.register

import com.starcode.registercontacts.data.repository.RegisterContactRepository
import com.starcode.registercontacts.domain.useCases.RegisterContactUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class RegisterContactModule {

    @Provides
    fun registerUseCaseProvider(registerContactRepository: RegisterContactRepository) =
        RegisterContactUseCase(registerContactRepository)

}