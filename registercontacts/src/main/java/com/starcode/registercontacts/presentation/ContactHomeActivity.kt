package com.starcode.registercontacts.presentation

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.starcode.registercontacts.R
import com.starcode.registercontacts.presentation.utils.permissionsList
import com.starcode.registercontacts.presentation.utils.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactHomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_home)

        if (!allPermissionsGranted()) {
            requestMultiplePermissions.launch(permissionsList())
        }
    }

    private fun allPermissionsGranted() = permissionsList().all {
        ContextCompat.checkSelfPermission(
            applicationContext, it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private val requestMultiplePermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            permissions.entries.forEach {
                Log.d("tag", "permissionts ${it.value}")
                if(!it.value)  showToast(getString(R.string.text_permissions_error), applicationContext)
            }

        }

}