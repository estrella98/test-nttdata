package com.starcode.registercontacts.presentation.list.holders

import android.view.View
import androidx.fragment.app.FragmentActivity
import com.starcode.registercontacts.R
import com.starcode.registercontacts.databinding.ItemContactBinding
import com.starcode.registercontacts.domain.model.Contact
import com.starcode.registercontacts.presentation.utils.showImage
import com.xwray.groupie.viewbinding.BindableItem

class ContactViewHolder(val activity:FragmentActivity, val item:Contact, val listener: ContactListener): BindableItem<ItemContactBinding>() {

    override fun bind(viewBinding: ItemContactBinding, position: Int) {
        viewBinding.apply {
            name.text = item.fullName
            documentNumber.text = item.documentNumber
            email.text = item.email
            phoneNumber.text = item.phoneNumber
            birthday.text = item.birthday
            item.photo?.let {
                image.showImage(activity, it)
            }
            delete.setOnClickListener { listener.delete() }
        }
    }

    override fun getLayout()= R.layout.item_contact

    override fun initializeViewBinding(view: View): ItemContactBinding = ItemContactBinding.bind(view)
}