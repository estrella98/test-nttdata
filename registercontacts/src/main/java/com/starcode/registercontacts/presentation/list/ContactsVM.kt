package com.starcode.registercontacts.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.starcode.registercontacts.domain.model.Contact
import com.starcode.registercontacts.domain.useCases.GetAllContactsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactsVM @Inject constructor(private val getAllContactsUseCase: GetAllContactsUseCase) :
    ViewModel() {


    private val disposable = CompositeDisposable()

    val contactsList: LiveData<List<Contact>>
        get() = LiveDataReactiveStreams.fromPublisher(getAllContactsUseCase.invoke())

    fun deleteContact(it: Contact) {
        viewModelScope.launch(Dispatchers.IO) { getAllContactsUseCase.deleteContact(it) }
    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}