package com.starcode.registercontacts.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.starcode.registercontacts.databinding.FragmentContactsBinding
import com.starcode.registercontacts.presentation.list.holders.ContactListener
import com.starcode.registercontacts.presentation.list.holders.ContactViewHolder
import com.starcode.registercontacts.presentation.utils.hide
import com.starcode.registercontacts.presentation.utils.show
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ContactsFragment : Fragment() {

    private val binding: FragmentContactsBinding by lazy {
        FragmentContactsBinding.inflate(layoutInflater)
    }
    private val viewModel: ContactsVM by viewModels()

    private val groupieAdapter = GroupieAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpBindings()
        setUpAdapter()
        setUpObservers()

    }


    private fun setUpBindings() {
        binding.apply {
            registerU.setOnClickListener {
                findNavController().navigate(ContactsFragmentDirections.actionContactsFragmentToLoginFragment())
            }

            swipeContainer.setOnRefreshListener {
                GlobalScope.launch {
                    delay(800)
                    swipeContainer.isRefreshing = false
                }
            }

        }

}

private fun setUpObservers() {
    viewModel.contactsList.observe(viewLifecycleOwner, {
        if (it.isEmpty()) binding.tvNoRecords.show()
        else binding.tvNoRecords.hide()
        val newList = it.map { ContactViewHolder(requireActivity(), it, object :ContactListener{
            override fun delete() {
               viewModel.deleteContact(it)
            }
        }) }
        groupieAdapter.updateAsync(newList)
    })
}

private fun setUpAdapter() {
    val mLayoutManager = LinearLayoutManager(requireContext())
    binding.rvContacts.apply {
        layoutManager = mLayoutManager
        adapter = groupieAdapter
    }
}
}