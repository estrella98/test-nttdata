package com.starcode.registercontacts.presentation.register

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.starcode.registercontacts.R
import com.starcode.registercontacts.databinding.FragmentRegisterContactBinding
import com.starcode.registercontacts.presentation.list.ContactsFragmentDirections
import com.starcode.registercontacts.presentation.utils.showImage
import com.starcode.registercontacts.presentation.utils.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterContactFragment : Fragment() {

    private val binding: FragmentRegisterContactBinding by lazy {
        FragmentRegisterContactBinding.inflate(layoutInflater)
    }

    private val viewModel: RegisterContactVM by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpBindings()
        setUpObservers()
    }


    private fun setUpBindings() {
        binding.apply {
            back.setOnClickListener {
                findNavController().popBackStack()
            }

            save.setOnClickListener {
                saveInfoOfContact()
            }

            uploadPhoto.setOnClickListener {
                takePhoto()
            }
        }
    }

    private fun setUpObservers() {
        viewModel.contactRegister.observe(viewLifecycleOwner, {
            if (!it) showToast(getString(R.string.text_contact_register_fail), requireContext())
            else findNavController().popBackStack()
        })
    }


    private fun saveInfoOfContact() {
        cleanErrors()

        binding.apply {

            val fullNameText = edFullName.text
            val documentText = edDocumentNumber.text
            val emailText = edEmail.text
            val phoneText = edPhoneNumber.text
            val birthdayText = edBirthday.text

            if (TextUtils.isEmpty(fullNameText)) {
                fullName.error = getString(R.string.text_ed_incomplete)
                return
            }
            if (TextUtils.isEmpty(documentText)) {
                documentNumber.error = getString(R.string.text_ed_incomplete)
                return
            }
            if (TextUtils.isEmpty(emailText)) {
                email.error = getString(R.string.text_ed_incomplete)
                return
            }
            if (TextUtils.isEmpty(phoneText)) {
                phoneNumber.error = getString(R.string.text_ed_incomplete)
                return
            }
            if (TextUtils.isEmpty(birthdayText)) {
                birthday.error = getString(R.string.text_ed_incomplete)
                return
            }

            viewModel.saveContact(
                fullNameText.toString(),
                documentText.toString(),
                emailText.toString(),
                phoneText.toString(),
                birthdayText.toString()
            )

        }
    }

    private fun cleanErrors() {
        binding.apply {
            fullName.error = null
            documentNumber.error = null
            email.error = null
            phoneNumber.error = null
            birthday.error = null
        }
    }

    private fun takePhoto() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        resultLauncherGallery.launch(intent)
    }

    private var resultLauncherGallery =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val path = result.data?.data?.path?.replace("/raw/", "")
                val uri: Uri? = result?.data?.data
                Log.d("RegisterContactFragment", "path: $path")
                if (uri != null) binding.image.showImage(requireActivity(), uri)
                else binding.image.showImage(requireActivity(), path)
                viewModel.saveImage(path)
            }
        }
}