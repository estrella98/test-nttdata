package com.starcode.registercontacts.presentation.list

import com.starcode.registercontacts.data.repository.RegisterContactRepository
import com.starcode.registercontacts.domain.useCases.GetAllContactsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class ContactsModule {

    @Provides
    fun getAllContactsUseCaseProvider(registerContactRepository: RegisterContactRepository) =
        GetAllContactsUseCase(registerContactRepository)

}