package com.starcode.registercontacts.presentation.list.holders

interface ContactListener {
    fun delete()
}